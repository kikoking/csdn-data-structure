#include"SeqList.h"

void SeqListInit(SEQ* pq)//初始化顺序表
{
    assert(pq);//断言如果为真，就成功通过
    //如果为假，即pq为空，则不通过
    //断言的好处：不用分析，直接告诉你哪里出错了
    pq->a = NULL;
    pq->size = 0;
    pq->capacity = 0;
}
void SeqListDestroy(SEQ* pq)//销毁顺序表
{
    free(pq->a);//释放动态数组空间
    pq->a = NULL;//将指示动态数组的指针置空
    pq->size = 0;//将有效数据个数置0
    pq->capacity = 0;//将容量个数置0
}
void SeqListPrint(SEQ* pq) //打印顺序表
{
    assert(pq);//声明；防止pq空指针
    for (int i = 0; i < pq->size; ++i)
    {
        printf("%d", pq->a[i]);//防止pq空指针
    }
    printf("\n");
}
void SeqCheckCapcity(SEQ* pq)//检测动态数组是否需要扩容
{
    assert(pq);
    //满了，需要扩容
    if (pq->size == pq->capacity)
    {
        int newcapacity = pq->capacity == 0 ? 4 : pq->capacity * 2;
        //如果动态容量为0，那么将其分配4个空间；否则将其扩容为原先的两倍
        DataType* newA = realloc(pq->a, sizeof(DataType) * newcapacity);
        //为其申请一个新的空间，大小为原先的两倍
        if (newA == NULL)//如果新空间的地址为空
        {
            printf("realloc fail\n");//申请空间失败
            exit(-1);
        }
        pq->a = newA;//将新申请的地址赋予给指针a
        pq->capacity = newcapacity;//将新申请的空间赋予给capacity
    }
}
void SeqListPushBack(SEQ* pq, DataType x)
{
    assert(pq);//声明，预防空指针
    SeqCheckCapcity(pq);//检测动态数组是否需要扩容
    pq->a[pq->size] = x;//尾插数据
    pq->size++;
}
void SeqListPushFront(SEQ* pq, DataType x)//头插数据
{
    assert(pq);//声明，防止空指针
    SeqCheckCapcity(pq);//检测动态数组是否需要扩容
    for (int i = pq->size; i > 0; --i)
    {
        pq->a[i] = pq->a[i - 1];
    }
    pq->a[0] = x;
    pq->size++;
}
void SeqListInsert(SEQ* pq, int pos, DataType x)//pos为数组的下标，插在第pos个数组下标初
{
    assert(pq);
    assert(pos >= 0 && pos <= pq->size);//保证pos的值最小为0，最大为pq->size
    SeqCheckCapcity(pq);
    for (int i = pq->size; i > pos; --i)
    {
        pq->a[i] = pq->a[i - 1];//从后往前，挨个向后移动一位，留出数组下标为pos处的空位
    }
    pq->a[pos] = x;
    pq->size++;
}
void SeqListPopBack(SEQ* pq)
{
    assert(pq);//防止空指针
    assert(pq->size > 0);
    //声明顺序表中有效数据至少有1位才可以进行尾删
    --pq->size;
}
void SeqListPopFront(SEQ* pq)
{
    assert(pq);
    assert(pq->size > 0);
    for (int i = 0; i < pq->size - 1; ++i)
    {
        pq->a[i] = pq->a[i + 1];
    }
    --pq->size;
}
void SeqListDelete(SEQ* pq, int pos)//删除数组下标为pos的值
{
    assert(pq);
    assert(pos >= 0 && pos < pq->size);//保证删除的数组下标由0-(pq->size-1)
    for (int i = pos; i < pq->size - 1; ++i)
    {
        pq->a[i] = pq->a[i + 1];//从前往后，挨个前移
    }
    --pq->size;
}
int SeqListFind(SEQ* pq, DataType x)
//按值查找，返回其对应的第一个数组下标值
{
    assert(pq);
    for (int i = 0; i < pq->size; ++i)
    {
        if (pq->a[i] == x)
            return i;
    }
    return -1;
}
void SeqListModify(SEQ* pq, int pos, DataType x)
{
    assert(pq);
    assert(pos >= 0 && pos < pq->size);//限制修改范围
    pq->a[pos] = x;
}