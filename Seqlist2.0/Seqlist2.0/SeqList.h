#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
typedef int DataType;//将int重定义为DataType
//下次想更改数据类型的时候只需要更改前面的int就可以，无需改变整个程序
typedef struct SeqList //定义一个顺序表的结构体
{
    DataType* a;//指示动态分配数组的指针
    DataType size;//数组中有效数据的个数
    DataType capacity;//数组的容量
}SEQ;// 将SeqList这个结构体重定义为SEQ，为了后续方便写程序
void SeqListInit(SEQ* pq);
void SeqListDestroy(SEQ* pq);
void SeqListPrint(SEQ* pq);
void SeqCheckCapcity(SEQ* pq);
void SeqListPushBack(SEQ* pq, DataType x);
void SeqListPushFront(SEQ* pq, DataType x);
void SeqListInsert(SEQ* pq, int pos, DataType x);
void SeqListPopBack(SEQ* pq);
void SeqListPopFront(SEQ* pq);
void SeqListDelete(SEQ* pq, int pos);
int  SeqListFind(SEQ* pq, DataType x);
void SeqListModify(SEQ* pq, int pos, DataType x);
