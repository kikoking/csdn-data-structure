#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

void menu()
{
    printf("*********************\n");
    printf("1.尾插数据    2.头插数据\n");
    printf("3.尾删数据    4.头删数据\n");
    printf("5.查找数据    6.打印数据\n");
    printf("0.退出程序 \n");
    printf("*********************\n");
}
int main()
{
    SEQ s;
    SeqListInit(&s);
    int option = 0;
    int x = 0;
    do {
        menu();
        scanf("%d", &option);
        switch (option)
        {
        case 1:
            printf("请输入数据，以-1结束：\n");
            while (1)
            {
                printf("请输入：");
                scanf("%d", &x);
                if (x == -1)
                    break;//跳出当前while循环
                SeqListPushBack(&s, x);
            }
            break;
        case 2:
            printf("请输入数据，以-1结束：\n");
            while (1)
            {
                printf("请输入：");
                scanf("%d", &x);
                if (x == -1)
                    break;//跳出当前while循环
                SeqListPushFront(&s, x);
            }
            break;
        case 3:
            SeqListPopBack(&s);
            break;
        case 4:
            SeqListPopFront(&s);
            break;
        case 5:
            printf("输入想要查找的数字:");
            scanf("%d", &x);
            int num =SeqListFind(&s, x);
            printf("你想要查找的数字在数组中的下标为%d\n", num);
            break;
        case 6:
            SeqListPrint(&s);
            break;
        default:
            break;
        }
    } while (option != 0);
    return 0;
}
